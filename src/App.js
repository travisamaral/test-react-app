import React from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';

import './App.css';
import Login  from './components/Login';
import Orders  from './components/Orders';

export class App extends React.Component {
	render() {
		const { user } = this.state;

		return (
			<BrowserRouter>
				<switch>
					<Route path="/login" render={() => (
						user ?
							<Redirect to="/orders" /> :
							<Login />
					)} />
					<Route path="/orders" render={() => {
						user ?
							<Orders user={user}/> :
							<Redirect to="/login"/>
					}} />
				</switch>
			</BrowserRouter>
		);
	}
}

export default App;