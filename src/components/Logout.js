import React from 'react';
import { Redirect } from 'react-router';

import '../App.css';
import '../index.css';

export class Logout extends React.Component {
	render() {
		return (
			this.state.shouldRedirect ?
				<Redirect to={'/login'} />
				<a onClick={this.logout}>Logout</a>
		);
	},
	logout() {
		doLogoutStuff().then(() => {
			this.setState({shouldRedirect: true});
		});
	}
}

export default Logout;