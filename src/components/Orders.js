import React from 'react';

import '../App.css';
import '../index.css';

export class Orders extends React.Component {
	render() {
		return (
			<ul className="orders container">
				<li>Order 1</li>
				<li>Order 2</li>
				<li>Order 3</li>
				<li>Order 4</li>
			</ul>
		);
	}
}

export default Orders;