import React from 'react';
import * as firebase from 'firebase';

export class LoginForm extends React.Component {

	constructor(props) {
   		super(props);
   		this.state = {
   			email: '',
   			password: '',
   			hide: 'hide'
   		}
   		//Bind inputs for functions
   		this.onChange = this.onChange.bind(this);
   		this.loginClick = this.loginClick.bind(this);
   		this.signUpClick = this.signUpClick.bind(this);
	};

	// Monitor value of email & pass for state
	onChange(e) {
		this.setState({
			[e.target.name] : e.target.value
		})
	}

	// Login user to application
	loginClick(e) {
		const auth = firebase.auth();
		const promise = auth.signInWithEmailAndPassword(this.state.email, this.state.password);
		promise.catch(e => console.log(e.message));
	}

	// Logout user from application
	logoutClick() {
		firebase.auth().signOut();
	}

	// Sign up user
	signUpClick() {
		const auth = firebase.auth();
		const promise = auth.createUserWithEmailAndPassword(this.state.email, this.state.password);
		promise.catch(e => console.log(e.message));
	}

	// Monitor if user is logged in or not
	componentDidMount () {
    		this.removeListener = firebase.auth().onAuthStateChanged((firebaseUser) => {
			if(firebaseUser) {
				console.log(firebaseUser)
				this.setState({
					hide: '',
					firebaseUser
				})
			} else {
				this.setState({
					hide: 'hide'
				})
				console.log('not logged in');
			}
		})
    	}

	render() {
		return (
			<div className="container">
				<input type="email" className="mdl-textfield__input" id="email" name="email" placeholder="Enter Email Address" onChange={this.onChange} value={this.state.email} />
				<input type="password" className="mdl-textfield__input" id="password" name="password" placeholder="Enter Password" onChange={this.onChange} value={this.state.password}/>
				<button id="btnLogin" className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" onClick={this.loginClick}>Login</button>
				<button id="btnSignUp" className="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" onClick={this.signUpClick}>Sign Up</button>
				<button id="btnLogout" className={"mdl-button mdl-js-button mdl-button--raised " + this.state.hide} onClick={this.logoutClick}>Logout</button>
			</div>
		);
	}
}

export default LoginForm;