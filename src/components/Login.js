import React from 'react';

import logo from '../logo.svg';
import '../App.css';
import '../index.css';

import LoginForm from './LoginForm';

export class Login extends React.Component {
	render() {
		return (
			<div className="App">
				<div className="App-header">
					<img src={logo} className="App-logo" alt="logo" />
					<h2>React + Firebase Login</h2>
				</div>
				<LoginForm />
			</div>
		);
	}
}

export default Login;