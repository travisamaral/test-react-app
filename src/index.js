import React from 'react';
import { render } from 'react-dom';
import * as firebase from 'firebase';

import App from './App';

var config = {
	apiKey: "AIzaSyCkZB6sObicWaANTovo3hjmCo-nQCooG08",
	authDomain: "test-project-9f48b.firebaseapp.com",
	databaseURL: "https://test-project-9f48b.firebaseio.com",
	projectId: "test-project-9f48b",
	storageBucket: "test-project-9f48b.appspot.com",
	messagingSenderId: "108155627183"
};

firebase.initializeApp(config);

render(
	<App />,
	document.getElementById('root')
)