import React from 'react';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';

// var Main = require('./components/Main');
var Home = require('./components/Home');
var PageOne = require('./components/PageOne');

var routes = (
  <Router history={hashHistory}>
    <Route path='/' component={Home}>
      <IndexRoute component={Home} />
      <Route path='/pageone' component={PageOne}/>
    </Route>
  </Router>
);

module.exports = routes